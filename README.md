# quotrs

A simple web app for sharing quotes. Currently implemented is a basic REST API at `/quotes` with CRUD operations.

## Running

To run the web server:

1. ensure that `rustc` and `cargo` are installed (easiest via `rustup`)
2. `git clone https://github.com/rakenodiax/quotrs && cd quotrs`
3. `cargo run`
4. `curl http://localhost:5000/ping` should return `PONG`

## TODO

- implement user accounts and authentication
- restrict access to private quotes to the user who created them