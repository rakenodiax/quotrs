use std::ops::Deref;

#[derive(Deserialize, StateData, StaticResponseExtender)]
pub struct ResourceId {
    id: i64,
}

impl Deref for ResourceId {
    type Target = i64;

    fn deref(&self) -> &Self::Target {
        &self.id
    }
}
