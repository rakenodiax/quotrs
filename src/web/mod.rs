pub mod extractors;
pub mod handlers;
pub mod middleware;

use gotham::router::builder::{build_router, DefineSingleRoute, DrawRoutes};
use gotham::router::Router;
use web::extractors::ResourceId;
use web::middleware::DatabasePool;
use gotham::middleware::state::StateMiddleware;
use gotham::pipeline::single_middleware;
use gotham::pipeline::single::single_pipeline;

fn router() -> Router {
    let db_pool = DatabasePool::new();

    let pool_middleware = StateMiddleware::new(db_pool);

    let pipeline = single_middleware(pool_middleware);

    let (chain, pipelines) = single_pipeline(pipeline);

    build_router(chain, pipelines, |route| {
        route.get("/ping").to(handlers::ping);

        // Quote Resource
        route.associate("/quotes", |assoc| {
            assoc.get().to(handlers::quotes::get_all);
            assoc.post().to(handlers::quotes::create);
        });
        route.associate("/quotes/:id", |assoc| {
            let mut assoc = assoc.with_path_extractor::<ResourceId>();

            assoc.get().to(handlers::quotes::get_one);
            assoc.put().to(handlers::quotes::update);
            assoc.delete().to(handlers::quotes::delete);
        });
    })
}

pub fn run_server(addr: &'static str) {
    println!("Server listening at {}", addr);

    gotham::start(addr, router())
}
