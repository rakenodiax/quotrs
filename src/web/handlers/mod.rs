pub mod quotes;

use gotham::state::State;

pub fn ping(state: State) -> (State, &'static str) {
    (state, "PONG")
}
