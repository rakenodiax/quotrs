use db::establish_connection;
use db::models::quotes::{Quote, QuoteCreationData};
use db::schema::quotes;
use diesel::prelude::*;
use futures::{future, Future, Stream};
use gotham::handler::{HandlerFuture, IntoHandlerError};
use gotham::helpers::http::response::{create_empty_response, create_response};
use gotham::state::{FromState, State};
use hyper::{Body, Response, StatusCode};
use web::extractors::ResourceId;
use web::middleware::DatabasePool;

#[derive(Debug, Deserialize)]
pub struct NewQuoteData {
    pub body: String,
    pub attribution: Option<String>,
    // bool::default() == false
    #[serde(default)]
    pub public: bool,
}

impl QuoteCreationData for NewQuoteData {
    fn body(&self) -> &str {
        self.body.as_str()
    }
    fn attribution(&self) -> Option<&str> {
        self.attribution.as_ref().map(|s| s.as_str())
    }
    fn owner_id(&self) -> Option<i32> {
        None
    }
    fn public(&self) -> Option<bool> {
        Some(self.public)
    }
}

#[derive(Debug, Deserialize, AsChangeset)]
#[table_name = "quotes"]
pub struct UpdateQuoteData {
    pub id: i64,
    pub body: Option<String>,
    pub attribution: Option<String>,
    pub public: Option<bool>,
}

pub fn create(mut state: State) -> Box<HandlerFuture> {
    let f = Body::take_from(&mut state)
        .concat2()
        .then(|full_body| match full_body {
            Ok(valid_body) => {
                let parsed_data: NewQuoteData =
                    match ::serde_json::from_slice(valid_body.to_vec().as_slice()) {
                        Ok(parsed) => parsed,
                        Err(e) => return future::err((state, e.into_handler_error())),
                    };

                // We want to have the database pool locked for as short as possible.
                let created: Option<Quote> = {
                    let db_pool = DatabasePool::borrow_from(&state);
                    let pool = db_pool
                        .pool
                        .lock()
                        .expect("Failed to acquire lock on the database pool");
                    // We flatten the pool fetch and database access errors into an Option
                    // panicking while holding a mutex lock will poison the mutex, and we're returning a 500 either way so don't need to retain the error information
                    pool.get()
                        .ok()
                        .and_then(|conn| Quote::new(&conn, &parsed_data).ok())
                };

                let created = match created {
                    Some(created) => created,
                    None => {
                        let error = std::io::Error::last_os_error()
                            .into_handler_error()
                            .with_status(StatusCode::INTERNAL_SERVER_ERROR);
                        return future::err((state, error));
                    }
                };

                let response_body = ::serde_json::to_vec(&created).unwrap();
                let res = create_response(
                    &state,
                    StatusCode::CREATED,
                    ::mime::APPLICATION_JSON,
                    response_body,
                );
                future::ok((state, res))
            }
            Err(e) => future::err((state, e.into_handler_error())),
        });

    Box::new(f)
}

pub fn get_all(state: State) -> (State, Response<Body>) {
    let all_quotes: Vec<Quote> = {
        let db_pool = DatabasePool::borrow_from(&state);
        let quotes = {
            let pool = db_pool
                .pool
                .lock()
                .expect("Failed to acquire lock on the database pool");
            pool.get()
                .ok()
                .and_then(|conn| quotes::table.get_results(&conn).ok())
        };
        quotes.unwrap_or(vec![])
    };
    let serialized = ::serde_json::to_vec(&all_quotes).unwrap();

    let res = create_response(&state, StatusCode::OK, ::mime::APPLICATION_JSON, serialized);

    (state, res)
}

pub fn get_one(state: State) -> (State, Response<Body>) {
    let quote_id = **ResourceId::borrow_from(&state); // ResourceId will deref to an i64

    let db_result: Option<Quote> = {
        let db_pool = DatabasePool::borrow_from(&state);
        let pool = db_pool
            .pool
            .lock()
            .expect("Failed to acquire lock on the database pool");
        pool.get().ok().and_then(|conn| {
            Quote::get_one_by_id(&conn, quote_id)
                .ok()
                .and_then(|quote| quote)
        })
    };

    let res = match db_result {
        Some(found) => {
            let serialized = ::serde_json::to_vec(&found).unwrap();
            create_response(&state, StatusCode::OK, ::mime::APPLICATION_JSON, serialized)
        }
        None => create_empty_response(&state, StatusCode::NOT_FOUND),
    };

    (state, res)
}

pub fn update(mut state: State) -> Box<HandlerFuture> {
    let f = Body::take_from(&mut state)
        .concat2()
        .then(|full_body| match full_body {
            Ok(valid_body) => {
                let deserialized =
                    ::serde_json::from_slice(valid_body.to_vec().as_slice()).unwrap();

                let updated: Option<Quote> = {
                    let db_pool = DatabasePool::borrow_from(&state);
                    let pool = db_pool
                        .pool
                        .lock()
                        .expect("Failed to acquire lock on the database pool");
                    pool.get().ok().and_then(|conn| {
                        ::diesel::update(quotes::table)
                            .set(&deserialized)
                            .get_result(&conn)
                            .ok()
                    })
                };

                let updated = match updated {
                    Some(updated) => updated,
                    None => {
                        let error = std::io::Error::last_os_error()
                            .into_handler_error()
                            .with_status(StatusCode::INTERNAL_SERVER_ERROR);
                        return future::err((state, error));
                    }
                };

                let response_body = ::serde_json::to_vec(&updated).unwrap();
                let res = create_response(
                    &state,
                    StatusCode::OK,
                    ::mime::APPLICATION_JSON,
                    response_body,
                );
                future::ok((state, res))
            }
            Err(e) => future::err((state, e.into_handler_error())),
        });

    Box::new(f)
}

pub fn delete(state: State) -> (State, Response<Body>) {
    use db::schema::quotes::dsl::*;
    let quote_id = **ResourceId::borrow_from(&state);
    let conn = establish_connection();

    let res = {
        let db_pool = DatabasePool::borrow_from(&state);
        let pool = db_pool
            .pool
            .lock()
            .expect("Failed to acquire lock on the database pool");
        let res = pool.get().ok().and_then(|conn| {
            ::diesel::delete(quotes)
                .filter(id.eq(quote_id))
                .execute(&conn)
                .ok().and_then(|_| {

                Some(create_empty_response(&state, StatusCode::NO_CONTENT))})
            });
        match res {
            Some(res) => res,
            None => create_empty_response(&state, StatusCode::NOT_FOUND)
        }
    };

    (state, res)
}
