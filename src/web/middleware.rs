use db::PgPool;
use std::sync::{Arc, Mutex};
use db::init_pool;

/// A shared database connection pool
#[derive(Clone, StateData)]
pub struct DatabasePool {
    pub pool: Arc<Mutex<PgPool>>,
}

impl DatabasePool {
    pub fn new() -> DatabasePool {
        DatabasePool {
            pool: Arc::new(Mutex::new(init_pool()))
        }
    }
}