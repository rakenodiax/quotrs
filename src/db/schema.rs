table! {
    quotes (id) {
        id -> Int8,
        body -> Text,
        attribution -> Nullable<Text>,
        owner_id -> Int4,
        public -> Bool,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}
