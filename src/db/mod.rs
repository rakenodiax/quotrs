pub mod models;
pub mod schema;

use diesel::prelude::*;
use diesel::r2d2::{ConnectionManager, Pool};
use std::env;

lazy_static! {
    static ref DATABASE_URL: String = env::var("DATABASE_URL")
        .unwrap_or_else(|_| "postgres://quotrs@localhost/quotrs".to_string());
}

pub type PgPool = Pool<ConnectionManager<PgConnection>>;

/// Establishes a one-off connection to the database
pub fn establish_connection() -> PgConnection {
    PgConnection::establish(DATABASE_URL.as_str()).expect("Failed to connect to the database url")
}

pub fn init_pool() -> PgPool {
    let manager = ConnectionManager::<PgConnection>::new(DATABASE_URL.as_str());
    Pool::new(manager).expect("Failed to initialize database connection pool")
}
