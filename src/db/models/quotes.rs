use chrono::prelude::*;
use db::schema::quotes;
use diesel::pg::Pg;
use diesel::prelude::*;
use diesel::Connection;

#[derive(Debug, Queryable, Serialize)]
pub struct Quote {
    id: i64,
    body: String,
    attribution: Option<String>,
    owner_id: i32,
    public: bool,
    created_at: DateTime<Utc>,
    updated_at: DateTime<Utc>,
}

impl Quote {
    pub fn new(
        conn: &impl Connection<Backend = Pg>,
        data: &impl QuoteCreationData,
    ) -> Result<Quote, ::diesel::result::Error> {
        let new_quote = NewQuote::from(data);

        ::diesel::insert_into(quotes::table)
            .values(&new_quote)
            .get_result(conn)
    }

    pub fn get_one_by_id(
        conn: &impl Connection<Backend = Pg>,
        quote_id: i64,
    ) -> Result<Option<Quote>, ::diesel::result::Error> {
        use db::schema::quotes::dsl::*;

        quotes
            .filter(id.eq(quote_id))
            .get_result::<Quote>(conn)
            .optional()
    }

    // Accessors
    pub fn id(&self) -> i64 {
        self.id
    }

    pub fn body(&self) -> &str {
        self.body.as_str()
    }

    pub fn attribution(&self) -> Option<&str> {
        self.attribution.as_ref().map(|s| s.as_str())
    }

    pub fn owner_id(&self) -> i32 {
        self.owner_id
    }

    pub fn public(&self) -> bool {
        self.public
    }

    pub fn created_at(&self) -> DateTime<Utc> {
        self.created_at
    }

    pub fn updated_at(&self) -> DateTime<Utc> {
        self.updated_at
    }
}

#[derive(Debug, Insertable)]
#[table_name = "quotes"]
pub struct NewQuote<'me> {
    pub body: &'me str,
    pub attribution: Option<&'me str>,
    pub owner_id: Option<i32>, // Default is the `0` owner_id, which is an anonymous user
    pub public: Option<bool>,  // Defaults to private
}

impl<'me, D> From<&'me D> for NewQuote<'me>
where
    D: QuoteCreationData,
{
    fn from(data: &'me D) -> NewQuote<'me> {
        NewQuote {
            body: data.body(),
            attribution: data.attribution(),
            owner_id: data.owner_id(),
            public: data.public(),
        }
    }
}

pub trait QuoteCreationData {
    fn body(&self) -> &str;
    fn attribution(&self) -> Option<&str>;
    fn owner_id(&self) -> Option<i32>;
    fn public(&self) -> Option<bool>;
}
