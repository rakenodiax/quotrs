#![allow(proc_macro_derive_resolution_fallback)]
extern crate gotham;
#[macro_use]
extern crate gotham_derive;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate chrono;
extern crate hyper;
#[macro_use]
extern crate diesel;
extern crate futures;
extern crate mime;
extern crate serde_json;
#[macro_use]
extern crate lazy_static;

pub mod db;
pub mod web;

pub use web::run_server;
